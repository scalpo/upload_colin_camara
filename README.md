# README #

### Upload files in POO ###

* this is an external library in POO to upload files with a folder to test without architecture
* Version : 2.0.0
* Author : Pascal Camara and Sharon Colin

#### How do I get it ####

* Download the folder UPLOAD_COLIN_CAMARA from bitbucket
* You can test it without architecture (Or add the file Upload.class.php in your Class folder)
* How to run tests : 
[Test_Server](http://colin.etudiant-eemi.com/perso/3A/TP/upload_colin_camara/TEST/)

### What you can do ###
* upload one or multiple files
* resize images in small/medium/large size and the large is the real size of your images
* when you upload, you have the link of your files
* all the images are inside a folder 'uploaded_files' automaticaly created